#!/bin/bash

# You should list git submodules repositories in SUBMODULES variable here
# i.e.
# GIT_SUBMODULES="https://gitservera.com/groupa/project-a.git"
# GIT_SUBMODULES+=" https://gitserverb.com/groupb/project-b.git"

GIT_SUBMODULES="https://gitlab.com/osef1/osef-posix.git"
