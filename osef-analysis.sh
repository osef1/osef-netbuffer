#!/bin/bash

green=$(tput setaf 2)
red=$(tput setaf 1)
normal=$(tput sgr0)

display-result()
{
    if [ $? -eq "0" ]; then
        echo $1
        echo "    "${green}passed${normal}
    else
        echo $1
        echo "    "${red}failed${normal}
    fi
}

./analyze-lizard.sh -x "./osef-*" 1> /dev/null 2> /dev/null
display-result lizard

./analyze-vera++.sh */*.h */*.cpp 1> /dev/null 2> /dev/null
display-result vera++

./analyze-cpplint.sh */*.h */*.cpp 1> /dev/null 2> /dev/null
display-result cpplint

./analyze-cppcheck.sh -i */osef-posix/* 1> /dev/null 2> /dev/null
display-result cppcheck
