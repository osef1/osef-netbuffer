#ifndef OSEFNETBUFFERSESSION_H
#define OSEFNETBUFFERSESSION_H

#include "NetworkSession.h"
#include "NetBuffer.h"

namespace OSEF
{
    class NetBufferSession : public NetworkSession
    {
    public:
        explicit NetBufferSession(OSEF::NetworkEndpoint* ep);
        ~NetBufferSession() override = default;

        bool sendBuffer(const OSEF::NetBuffer& buffer);  ///< send buffer after session setup
        bool receiveBuffer(OSEF::NetBuffer& buffer);  ///< receive payload after session setup
        bool receiveBuffer(OSEF::NetBuffer& buffer, const timespec& to);  ///< receive payload after session setup

        NetBufferSession(const NetBufferSession&) = delete;  // copy constructor
        NetBufferSession& operator=(const NetBufferSession&) = delete;  // copy assignment
        NetBufferSession(NetBufferSession&&) = delete;  // move constructor
        NetBufferSession& operator=(NetBufferSession&&) = delete;  // move assignment

    protected:
        // session setup helpers
    //    bool sendDirectBuffer(const OSEF::NetBuffer& buffer);  ///< send buffer directly to endpoint, no session setup
    //    bool receiveDirectBuffer(OSEF::NetBuffer& buffer);  ///< receive buffer directly from endpoint, no session setup
    };
}  // namespace OSEF

#endif /* OSEFNETBUFFERSESSION_H */
