#include "NetBufferSession.h"

OSEF::NetBufferSession::NetBufferSession(OSEF::NetworkEndpoint* ep)
    :NetworkSession(ep) {}

//  bool OSEF::NetBufferSession::sendDirectBuffer(const OSEF::NetBuffer& buffer)
//{
//    bool ret = false;
//
//    if (endpoint != nullptr)
//    {
//        ret = endpoint->sendString(buffer.asString());
//    }
//
//    return ret;
//}

//  bool OSEF::NetBufferSession::receiveDirectBuffer(OSEF::NetBuffer& buffer)
//{
//    bool ret = false;
//
//    if (endpoint != nullptr)
//    {
//        if (endpoint->receiveString(buffer.getInnerString(), buffer.getBufferSize()))
//        {
//            ret = buffer.resetForExtraction(buffer.asString().size());
//        }
//    }
//
//    return ret;
//}

bool OSEF::NetBufferSession::sendBuffer(const OSEF::NetBuffer& buffer)
{
    const bool ret = sendString(buffer.asString());
    return ret;
}

bool OSEF::NetBufferSession::receiveBuffer(OSEF::NetBuffer& buffer)
{
    bool ret = false;

    if (receiveString(buffer.getInnerString(), buffer.getBufferSize()))
    {
        ret = buffer.resetForExtraction(buffer.asString().size());
    }

    return ret;
}

bool OSEF::NetBufferSession::receiveBuffer(OSEF::NetBuffer& buffer, const timespec& to)
{
    bool ret = false;

    if (receiveString(buffer.getInnerString(), buffer.getBufferSize(), to))
    {
        ret = buffer.resetForExtraction(buffer.asString().size());
    }

    return ret;
}
