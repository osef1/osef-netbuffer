#ifndef OSEFNETBUFFER_H
#define OSEFNETBUFFER_H

#include <string>

/// Socket network data buffer

/// NetBuffer provides simplified access read/write methods to data buffer
/// manual and automatic offsets are both available

namespace OSEF
{
    class NetBuffer
    {
    public:
        explicit NetBuffer(const size_t& buffsize, const size_t& offset = 0);
        virtual ~NetBuffer() = default;

        bool setUInt8(const uint8_t& d, const size_t& offset);  ///< set 8 bits data under frame minimum offset and does not update data length
        bool setUInt16(const uint16_t& d, const size_t& offset);  ///< set 16 bits data under frame minimum offset and does not update data length
        bool setUInt32(const uint32_t& d, const size_t& offset);  ///< set 32 bits data under frame minimum offset and does not update data length
        bool setUInt64(const uint64_t& d, const size_t& offset);  ///< set 64 bits data under frame minimum offset and does not update data length
        bool setCharacters(const std::string& s, const size_t& offset);  ///< set data to frame and update data length

        bool pushUInt8(const uint8_t& d);  ///< add 8 bits data to frame and update data length
        bool pushUInt16(const uint16_t& d);  ///< add 16 bits data to frame and update data length
        bool pushUInt32(const uint32_t& d);  ///< add 32 bits data to frame and update data length
        bool pushUInt64(const uint64_t& d);  ///< add 64 bits data to frame and update data length
        bool pushCharacters(const std::string& s);  ///< add data without end of string to frame and update data length
        bool pushString(const std::string& s, const uint8_t& e = eos);  ///< add data with end of string to frame and update data length

        size_t getBufferSize() const {return bufferSize;}
        size_t getDataLength() const {return dataLength;}
        bool isReadable() const;  ///< enough data were received to fill static area

        size_t getRemainingSizeToPush() const;
        size_t getRemainingSizeToExtract() const;

        bool getUInt8(uint8_t& d, const size_t& offset);  ///< get 8 bits data under frame minimum offset
        bool getUInt16(uint16_t& d, const size_t& offset);  ///< get 16 bits data under frame minimum offset
        bool getUInt32(uint32_t& d, const size_t& offset);  ///< get 32 bits data under frame minimum offset
        bool getUInt64(uint64_t& d, const size_t& offset);  ///< get 64 bits data under frame minimum offset

        bool extractUInt8(uint8_t& d);
        bool extractUInt16(uint16_t& d);
        bool extractUInt32(uint32_t& d);
        bool extractUInt64(uint64_t& d);
        bool extractCharacters(std::string& chars, const size_t& length);  ///< extracts l characters
        bool extractString(std::string& chars, const size_t& length, const int8_t& e = eos);  ///< extracts l characters or less if end of string character is found

        bool resetForSetup();  ///< set data length to extract offset base and reset offsets
        const std::string& asString() const {return buffer;}

        std::string& getInnerString() {return buffer;}
        bool resetForExtraction(const size_t& dl);  ///< set data length and reset offsets

        bool reset();  ///< reset data length and offsets to zero

        NetBuffer(const NetBuffer&) = delete;  // copy constructor
        NetBuffer& operator=(const NetBuffer&) = delete;  // copy assignment
        NetBuffer(NetBuffer&&) = delete;  // move constructor
        NetBuffer& operator=(NetBuffer&&) = delete;  // move assignment

    protected:
        virtual bool setLimits(const size_t& dl, const size_t& offset, const size_t& offsetmin);

        bool set(const void* src, const size_t& offset, const size_t& n);
        bool get(void* dst, const size_t& offset, const size_t& n);
        bool push(const void* src, const size_t& n);
        bool extract(void* dst, const size_t& n);

    private:
        bool increaseSetZone(const size_t& dl);
        bool increaseGetZone(const size_t& dl);

        static const int8_t eos;  ///< end of string character

        std::string buffer;
        size_t bufferSize;  ///< buffer full size
        size_t dataLength;  ///< current data size in buffer
        size_t extractOffset;  ///< current extraction offset;
        size_t extractOffsetMin;  ///< minimum extraction offset
        size_t extractOffsetBase;  ///< reset for extraction fallback offset
    };
}  // namespace OSEF

#endif /* OSEFNETBUFFER_H */
