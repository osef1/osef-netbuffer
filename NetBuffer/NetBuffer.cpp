#include "NetBuffer.h"
#include "Swap.h"
#include "Debug.h"

const int8_t OSEF::NetBuffer::eos = '\0';

/// Initializes buffer and offsets

/// buffer is set tzero after allocation
/// \param buffsize buffer size must be non-null
/// \param offset base offset should be lower than buffer size
OSEF::NetBuffer::NetBuffer(const size_t& buffsize, const size_t& offset)
    :bufferSize(buffsize),
    dataLength(0),
    extractOffset(0),
    extractOffsetMin(0),
    extractOffsetBase(offset) {}

/// checks and sets data length and offsets

/// \param dl data length
/// \param offset current extraction offset
/// \param offsetmin minimum extraction offset
bool OSEF::NetBuffer::setLimits(const size_t& dl, const size_t& offset, const size_t& offsetmin)
{
    bool ret = false;

    // check buffersize >= dataLength >= extractOffset >= extractOffsetMin
    if (dl <= bufferSize)
    {
        try
        {
            buffer.resize(dl, 0);
        }
        catch (...) {}

        if (buffer.size() == dl)
        {
            dataLength = dl;

            if (offset <= dataLength)
            {
                extractOffset = offset;
                if (offsetmin <= extractOffset)
                {
                    extractOffsetMin = offsetmin;
                    ret = true;
                }
                else
                {
                    DERR("minimum offset " << offsetmin << " greater than offset " << extractOffset);
                }
            }
            else
            {
                // happens each time buffer is resetted to data length lower than minimum offset
                DERR("offset " << offset << " greater than data length " << dataLength);
            }
        }
        else
        {
            DERR("error resizing buffer to " << dl);
        }
    }
    else
    {
        DERR("data length " << dl << " greater than buffer size " << bufferSize);
    }

    return ret;
}

/// sets data length and offsets to zero
bool OSEF::NetBuffer::reset()
{
    const bool ret = setLimits(0, 0, 0);
    return ret;
}

/// resets data length and offsets using offset base

/// meant to be called prior each data extraction
bool OSEF::NetBuffer::resetForExtraction(const size_t& dl)
{
    bool ret = false;

    if (dl >= extractOffsetBase)
    {
        ret = setLimits(dl, extractOffsetBase, extractOffsetBase);
    }
    else
    {
        ret = setLimits(dl, dl, dl);
    }

    return ret;
}

/// reset data length and offsets to offset base

/// meant to be called prior each data setup
bool OSEF::NetBuffer::resetForSetup()
{
    const bool ret = setLimits(extractOffsetBase, extractOffsetBase, extractOffsetBase);
    return ret;
}

/// checks and increases static zone

/// \param dl data length
/// static zone contains static fields at static offsets
/// and is available through direct access
bool OSEF::NetBuffer::increaseSetZone(const size_t& dl)
{
    bool ret = false;

    if (dl > extractOffsetMin)  // need to increase
    {
        if (dataLength == extractOffsetMin)  // no push occured yet
        {
            // increase data length and other limits if data length is already equal to offsetmin
            ret = setLimits(dl, dl, dl);
        }
    }
    else
    {
        ret = true;  // no need to increase
    }

    return ret;
}

/// copy static data at buffer offset

/// \param src pointer to source data
/// \param offset buffer offset to topy source data to
/// \param n source data size in bytes

// automatically increases static zone if necessary
bool OSEF::NetBuffer::set(const void* src, const size_t& offset, const size_t& n)
{
    bool ret = false;

    // increase static zone if necessary
    if (increaseSetZone(offset+n))
    {
        try
        {
            buffer.replace(offset, n, static_cast<const char*>(src), n);
            ret = true;
        }
        catch (...) {}
    }
    else
    {
        DERR("not enough room left in static area");
    }

    return ret;
}

bool OSEF::NetBuffer::increaseGetZone(const size_t& dl)
{
    bool ret = false;

    if (dl > extractOffsetMin)  // need to increase
    {
        if (extractOffset == extractOffsetMin)  // no extract occured yet
        {
            // increase offsetmin and offset if they are identical to a smaller valuethan datalength
            ret = setLimits(dataLength, dl, dl);
        }
    }
    else
    {
        ret = true;  // no need to increase
    }

    return ret;
}

bool OSEF::NetBuffer::get(void* dst, const size_t& offset, const size_t& n)
{
    bool ret = false;

    if (increaseGetZone(offset+n))
    {
        if (buffer.copy(static_cast<char*>(dst), n, offset) == n)
        {
            ret = true;
        }
        else
        {
            DERR("get data");
        }
    }
    else
    {
        DERR("static area overflow");
    }

    return ret;
}

bool OSEF::NetBuffer::push(const void* src, const size_t& n)
{
    bool ret = false;

    // increase data length only
    if (setLimits(dataLength+n, extractOffset, extractOffsetMin))
    {
        try
        {
            // data length has been increased, need to withdraw n
            const size_t offset = dataLength-n;
            buffer.replace(offset, n, static_cast<const char*>(src), n);
            ret = true;
        }
        catch (...) {}
    }
    else
    {
        DERR("not enough room left in arrayBuffer");
    }

    return ret;
}

bool OSEF::NetBuffer::extract(void* dst, const size_t& n)
{
    bool ret = false;

    // increase extract offset only
    if (setLimits(dataLength, extractOffset + n, extractOffsetMin))
    {
        // extract offset has been increased, need to withdraw n
        const size_t offset = extractOffset - n;
        if (buffer.copy(static_cast<char*>(dst), n, offset) == n)
        {
            ret = true;
        }
        else
        {
            DERR("get data");
        }
    }
    else
    {
        DERR("arrayBuffer overflow");
    }

    return ret;
}

bool OSEF::NetBuffer::setUInt8(const uint8_t& d, const size_t& offset)
{
    const bool ret = set(&d, offset, sizeof(uint8_t));
    return ret;
}

bool OSEF::NetBuffer::setUInt16(const uint16_t& d, const size_t& offset)
{
    uint16_t swap = hton16(d);
    const bool ret = set(&swap, offset, sizeof(uint16_t));
    return ret;
}

 bool OSEF::NetBuffer::setUInt32(const uint32_t& d, const size_t& offset)
{
    const uint32_t swap = hton32(d);
    const bool ret = set(&swap, offset, sizeof(uint32_t));
    return ret;
}

 bool OSEF::NetBuffer::setUInt64(const uint64_t& d, const size_t& offset)
{
    const uint64_t swap = hton64(d);
    const bool ret = set(&swap, offset, sizeof(uint64_t));
    return ret;
}

bool OSEF::NetBuffer::setCharacters(const std::string& s, const size_t& offset)
{
    const bool ret = set(s.c_str(), offset, s.size());
    return ret;
}

bool OSEF::NetBuffer::pushUInt8(const uint8_t& d)
{
    const bool ret = push(&d, sizeof(uint8_t));
    return ret;
}

bool OSEF::NetBuffer::pushUInt16(const uint16_t& d)
{
    uint16_t swap = hton16(d);
    const bool ret = push(&swap, sizeof(uint16_t));
    return ret;
}

bool OSEF::NetBuffer::pushUInt32(const uint32_t& d)
{
    const uint32_t swap = hton32(d);
    const bool ret = push(&swap, sizeof(uint32_t));
    return ret;
}

 bool OSEF::NetBuffer::pushUInt64(const uint64_t& d)
{
    const uint64_t swap = hton64(d);
    const bool ret = push(&swap, sizeof(uint64_t));
    return ret;
}

bool OSEF::NetBuffer::pushCharacters(const std::string& s)
{
    const bool ret = push(s.c_str(), s.size());
    return ret;
}

bool OSEF::NetBuffer::pushString(const std::string& s, const uint8_t& e)
{
    bool ret = false;

    if (pushCharacters(s))
    {
        ret = pushUInt8(e);
    }

    return ret;
}

bool OSEF::NetBuffer::getUInt8(uint8_t& d, const size_t& offset)
{
    const bool ret = get(&d, offset, sizeof(uint8_t));
    return ret;
}

bool OSEF::NetBuffer::getUInt16(uint16_t& d, const size_t& offset)
{
    bool ret = false;

    uint16_t swap = 0;
    if (get(&swap, offset, sizeof(uint16_t)))
    {
        d = hton16(swap);
        ret = true;
    }

    return ret;
}

 bool OSEF::NetBuffer::getUInt32(uint32_t& d, const size_t& offset)
{
    bool ret = false;

    uint32_t swap = 0;
    if (get(&swap, offset, sizeof(uint32_t)))
    {
        d = hton32(swap);
        ret = true;
    }

    return ret;
}

 bool OSEF::NetBuffer::getUInt64(uint64_t& d, const size_t& offset)
{
    bool ret = false;

    uint64_t swap = 0;
    if (get(&swap, offset, sizeof(uint64_t)))
    {
        d = hton64(swap);
        ret = true;
    }

    return ret;
}

bool OSEF::NetBuffer::extractUInt8(uint8_t& d)
{
    const bool ret = extract(&d, sizeof(uint8_t));

    return ret;
}

bool OSEF::NetBuffer::extractUInt16(uint16_t& d)
{
    bool ret = false;

    uint16_t swap = 0;
    if (extract(&swap, sizeof(uint16_t)))
    {
        d = hton16(swap);
        ret = true;
    }

    return ret;
}

bool OSEF::NetBuffer::extractUInt32(uint32_t& d)
{
    bool ret = false;

    uint32_t swap = 0;
    if (extract(&swap, sizeof(uint32_t)))
    {
        d = hton32(swap);
        ret = true;
    }

    return ret;
}

bool OSEF::NetBuffer::extractUInt64(uint64_t& d)
{
    bool ret = false;

    uint64_t swap = 0;
    if (extract(&swap, sizeof(uint64_t)))
    {
        d = hton64(d);
        ret = true;
    }

    return ret;
}

bool OSEF::NetBuffer::extractCharacters(std::string& chars, const size_t& length)
{
    bool ret = false;

    // increase extract offset only
    if (setLimits(dataLength, extractOffset + length, extractOffsetMin))
    {
        // extract offset has been increased, need to withdraw l
        const size_t offset = extractOffset - length;
        chars = buffer.substr(offset, length);
        ret = true;
    }
    else
    {
        DERR("arrayBuffer overflow");
    }

    return ret;
}

bool OSEF::NetBuffer::extractString(std::string& chars, const size_t& length, const int8_t& e)
{
    bool ret = false;

    size_t p = buffer.find(e, extractOffset);
    if (p != std::string::npos)
    {
        if (p <= (extractOffset + length))
        {
            if (extractCharacters(chars, p-extractOffset))
            {
                ret = setLimits(dataLength, extractOffset + 1UL, extractOffsetMin);
            }
        }
        else
        {
            DWARN("end of string character found beyond allowed length " << (p - extractOffset) << " > " << length);
        }
    }
    else
    {
        DWARN("could not find end of string character \'" << e <<"\'");
    }

    return ret;
}

size_t OSEF::NetBuffer::getRemainingSizeToPush() const
{
    size_t ret = 0;

    if (bufferSize >= dataLength)
    {
        ret = bufferSize-dataLength;
    }

    return ret;
}

size_t OSEF::NetBuffer::getRemainingSizeToExtract() const
{
    size_t ret = 0;

    if (dataLength >= extractOffset)
    {
        ret = dataLength-extractOffset;
    }

    return ret;
}

bool OSEF::NetBuffer::isReadable() const
{
    const bool ret = (dataLength >= extractOffsetBase);
    return ret;
}
