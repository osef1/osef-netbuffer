#ifndef OSEFNETBUFFERP_H
#define OSEFNETBUFFERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t size;
        size_t offset;
    }NetBufferParam;

    class NetBufferP : public ::testing::TestWithParam<NetBufferParam>
    {
        protected:
            NetBufferP() {}
            ~NetBufferP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFNETBUFFERP_H
