#include "NetBufferP.h"
#include "NetBuffer.h"
#include "Randomizer.h"

#include <string>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(MinStatic,
                             NetBufferP,
                             testing::Values(NetBufferParam({1, 0})));

    INSTANTIATE_TEST_SUITE_P(MinDynamic,
                             NetBufferP,
                             testing::Values(NetBufferParam({1, 1})));

    INSTANTIATE_TEST_SUITE_P(MinSplit,
                             NetBufferP,
                             testing::Values(NetBufferParam({2, 1})));

    INSTANTIATE_TEST_SUITE_P(SmallNoOffset,
                             NetBufferP,
                             testing::Values(NetBufferParam({128, 0})));

    INSTANTIATE_TEST_SUITE_P(SmallAndOffset,
                             NetBufferP,
                             testing::Values(NetBufferParam({128, 32})));

    void checkInitialValues(NetBuffer& buffer, const NetBufferParam& param)
    {
        EXPECT_EQ(buffer.getBufferSize(), param.size);
        EXPECT_EQ(buffer.getDataLength(), static_cast<size_t>(0));

        if (param.offset == 0)
        {
            EXPECT_TRUE(buffer.isReadable());
        }
        else
        {
            EXPECT_FALSE(buffer.isReadable());
        }

        EXPECT_EQ(buffer.getRemainingSizeToPush(), param.size);
        EXPECT_EQ(buffer.getRemainingSizeToExtract(), static_cast<size_t>(0));

        EXPECT_EQ(buffer.asString(), "");
        EXPECT_EQ(buffer.getInnerString(), "");
    }

    TEST_P(NetBufferP, doNothing)
    {
        NetBuffer buffer(GetParam().size, GetParam().offset);

        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, getU8Fail)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_FALSE(buffer.getUInt8(data, 0));

        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, setU8Fail)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_FALSE(buffer.setUInt8(data, GetParam().size));

        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, setU8)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_TRUE(buffer.setUInt8(data, 0));

        EXPECT_TRUE(buffer.reset());
        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, setSameTwiceU8)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_TRUE(buffer.setUInt8(data, 0));

        EXPECT_TRUE(buffer.setUInt8(data, 0));

        EXPECT_TRUE(buffer.reset());
        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, getU8)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_TRUE(buffer.setUInt8(data, 0));

        uint8_t read = ~data;
        EXPECT_TRUE(buffer.getUInt8(read, 0));
        EXPECT_EQ(read, data);

        EXPECT_TRUE(buffer.reset());
        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, getSameTwiceU8)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_TRUE(buffer.setUInt8(data, 0));

        uint8_t read = ~data;
        EXPECT_TRUE(buffer.getUInt8(read, 0));
        EXPECT_EQ(read, data);

        data = 1;
        EXPECT_TRUE(buffer.setUInt8(data, 0));

        read = ~data;
        EXPECT_TRUE(buffer.getUInt8(read, 0));
        EXPECT_EQ(read, data);

        EXPECT_TRUE(buffer.reset());
        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, extractU8Fail)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_FALSE(buffer.extractUInt8(data));

        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, pushU8)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_TRUE(buffer.pushUInt8(data));

        EXPECT_TRUE(buffer.reset());
        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, extractU8)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        uint8_t data = 0;
        EXPECT_TRUE(buffer.pushUInt8(data));

        uint8_t read = ~data;
        EXPECT_TRUE(buffer.extractUInt8(read));
        EXPECT_EQ(read, data);

        EXPECT_TRUE(buffer.reset());
        checkInitialValues(buffer, GetParam());
    }

    TEST_P(NetBufferP, resetForExtractionNull)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        EXPECT_TRUE(buffer.resetForExtraction(0));

        checkInitialValues(buffer, GetParam());
    }

    std::string getRandomPayload(const size_t& size)
    {
        std::string ret = "";

        Randomizer randomizer;
        for (size_t i = 0; i < size; i++)
        {
            ret += randomizer.getUInt8();
        }

        return ret;
    }

    TEST_P(NetBufferP, resetForExtractionFull)
    {
        OSEF::NetBuffer buffer(GetParam().size, GetParam().offset);

        std::string payload = getRandomPayload(GetParam().size);
        buffer.getInnerString() = payload;

        EXPECT_TRUE(buffer.resetForExtraction(GetParam().size));

        for (size_t i = 0; i < GetParam().offset; i++)
        {
            uint8_t data = 0;
            EXPECT_TRUE(buffer.getUInt8(data, i));
            EXPECT_EQ(data, static_cast<uint8_t>(payload.at(i)));
        }

        for (size_t i = GetParam().offset; i < GetParam().size; i++)
        {
            uint8_t data = 0;
            EXPECT_TRUE(buffer.extractUInt8(data));
            EXPECT_EQ(data, static_cast<uint8_t>(payload.at(i)));
        }

        EXPECT_TRUE(buffer.reset());
        checkInitialValues(buffer, GetParam());
    }
}  // namespace OSEF
