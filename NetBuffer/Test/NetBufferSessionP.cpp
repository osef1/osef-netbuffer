#include "NetBufferSessionP.h"
#include "NetBufferSession.h"

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(SmallNoOffset,
                             NetBufferSessionSessionP,
                             testing::Values(NetBufferSessionSessionParam({128, 0})));

    TEST_P(NetBufferSessionSessionP, doNothing)
    {
        OSEF::NetBufferSession session(nullptr);
    }
}  // namespace OSEF
