#ifndef OSEFNETBUFFERSESSIONP_H
#define OSEFNETBUFFERSESSIONP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t size;
        size_t offset;
    }NetBufferSessionSessionParam;

    class NetBufferSessionSessionP : public ::testing::TestWithParam<NetBufferSessionSessionParam>
    {
        protected:
            NetBufferSessionSessionP() {}
            ~NetBufferSessionSessionP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFNETBUFFERSESSIONP_H
