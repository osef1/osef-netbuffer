#!/bin/bash

# get submodules repositories list
. ./git-get-repositories.sh

# remove submodules
for URL in $GIT_SUBMODULES
do
    BASENAME=$(basename $URL)
    FILENAME=${BASENAME%.*}
    #EXTENSION=${BASENAME##*.}

    echo "removing submodule $FILENAME"
    echo

    git submodule deinit --quiet -f $FILENAME
    git rm --quiet -f $FILENAME
#    git rm --cached --quiet $FILENAME
    rm -rf .git/modules/$FILENAME
done

# URL https://gitlab.com/fredloreaud/osef-log
# BASENAME osef-log
# FILENAME osef-log
# EXTENSION osef-log

# URL git@gitlab.com:fredloreaud/osef-log.git
# BASENAME osef-log.git
# FILENAME osef-log
# EXTENSION git

# add submodules
for URL in $GIT_SUBMODULES
do
    echo "adding submodule $URL"
    echo

    git submodule add $URL
done

git submodule update --recursive --init
