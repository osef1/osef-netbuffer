cmake_minimum_required(VERSION 3.13)

include(osef.cmake)

project(osef-posix-netbuffer)

add_library(${PROJECT_NAME} INTERFACE)

if (NETBUFFER)
  set(ALL "off")
else ()
  set(ALL "on")
endif ()

# Replace test by global-test
if (ALL AND TEST)
    # Reset test to avoid building subdirectories tests
    SET(TEST "off")
    SET(GLOBAL-TEST "on")
    
    add_subdirectory("osef-gtest" "${CMAKE_CURRENT_BINARY_DIR}/gtest")
    link_directories(${CMAKE_CURRENT_BINARY_DIR}/gtest)

    add_subdirectory("osef-posix/Mutex" "${CMAKE_CURRENT_BINARY_DIR}/Mutex")
    link_directories(${CMAKE_CURRENT_BINARY_DIR}/Mutex)

    # Call all test suites from subdirectories
    enable_testing ()
    add_test(NAME osef-posix-netbuffer-gtest-suite  
            COMMAND osef-netbuffer-gtest)
endif ()

if (NETBUFFER OR ALL)
  add_subdirectory(NetBuffer)
endif ()

target_link_libraries(${PROJECT_NAME} INTERFACE osef-netbuffer)
